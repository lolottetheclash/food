(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
const Food = require('./src/models/food')
const nutriment = (plat, nutriment) => parseFloat(
  Array.from(plat.querySelectorAll('dt'))  // find all dt's in plat
    .find(el => el.innerHTML.toLowerCase() === nutriment) // keep the one with the nutriment
    .nextElementSibling // get to the dd
    .innerHTML) 

const glucides = plat => nutriment(plat, 'glucides')
const lipides = plat => nutriment(plat, 'lipides')
const calories = plat => nutriment(plat, 'calories')
const proteines = plat => nutriment(plat, 'protéines')

const platEl2js = (plat) => new Food({
  name: plat.querySelector('p').innerHTML,
  weight: parseFloat(plat.attributes['data-weight'].value),
  lipides: lipides(plat),
  glucides: glucides(plat),
  proteines: proteines(plat)
})

window.plats = Array.from(document.querySelectorAll('.plat')).map(platEl2js)



},{"./src/models/food":2}],2:[function(require,module,exports){
class Food{
  constructor(nameOrObject, weight, caloriesTotal, lipides, glucides, proteines){
    if (nameOrObject instanceof Object) {
      Object.assign(this, nameOrObject)
    } else {
      this.name = nameOrObject
      this.weight = weight
      this.lipides = lipides
      this.glucides = glucides
      this.proteines = proteines
    }
  }
  get calories () { return this.lipides*9+this.glucides*4+this.proteines*4}
}

Object.defineProperty(Food.prototype,'caloriesTotal', {
  get: function(){
    return  this.calories * this.weight / 100
  }
})



Food.message = 'this is in food module'

module.exports = Food 


},{}]},{},[1]);
